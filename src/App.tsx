import { redirect } from 'react-router-dom';
import {
  CssBaseline,
  LinearProgress,
  ThemeProvider,
  createTheme,
} from '@mui/material';
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { LocalizationProvider } from '@mui/x-date-pickers';
import useStore from './store';
import Snackbar from './components/common/Snackbar';
import Home from './pages/Home';
import { isAuthenticated } from './services/apiService';
import SignUp from './pages/SignUp';
import LoginPage from './pages/Login';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';

const darkTheme = createTheme({
  palette: {
    mode: 'dark',
  },
});

const router = createBrowserRouter([
  {
    path: '/',
    loader: async () => {
      const isAuth = isAuthenticated();
      if (isAuth) {
        return null;
      }
      return redirect('/login');
    },
    element: <Home />,
  },
  {
    path: '/login',
    element: <LoginPage />,
  },
  {
    path: '/signup',
    element: <SignUp />,
  },
  {
    path: '/logout',
    async action() {
      return redirect('/signin');
    },
  },
]);

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      retry: 0,
    },
  },
});

function App() {
  const loading = useStore((state) => state.loading);
  const snackbarState = useStore((state) => state.snackbarState);
  const setSnackbarState = useStore((state) => state.setSnackbarState);

  return (
    <QueryClientProvider client={queryClient}>
      <ThemeProvider theme={darkTheme}>
        <CssBaseline enableColorScheme />
        {loading && <LinearProgress />}
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <RouterProvider router={router} />
          {snackbarState && (
            <Snackbar
              message={snackbarState.message}
              severity={snackbarState.severity}
              onClose={() => setSnackbarState(null)}
            />
          )}
        </LocalizationProvider>
      </ThemeProvider>
    </QueryClientProvider>
  );
}

export default App;
