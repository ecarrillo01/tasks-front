import { Container, Typography } from '@mui/material';
import { PropsWithChildren } from 'react';

export default function AuthLayout({
  children,
  title,
}: { title: string } & PropsWithChildren) {
  return (
    <Container component="main" sx={{ marginTop: 8 }} maxWidth="xs">
      <Typography component="h1" variant="h5">
        {title}
      </Typography>
      {children}
    </Container>
  );
}
