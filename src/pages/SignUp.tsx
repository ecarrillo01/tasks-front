import { Grid, Typography, TextField, Button } from '@mui/material';
import { AxiosError } from 'axios';
import useStore from '../store';
import { useNavigate, Link } from 'react-router-dom';
import { signUp } from '../services/apiService';
import { useForm, Controller } from 'react-hook-form';
import { zodResolver } from '@hookform/resolvers/zod';
import { z } from 'zod';
import AuthLayout from '../layouts/AuthLayout';
import { isStrongPassword } from 'validator';

const registerSchema = z
  .object({
    name: z.string({ required_error: 'name is required' }),
    email: z
      .string({ required_error: 'email is required' })
      .email({ message: 'Invalid email address' }),
    password: z
      .string({
        required_error: 'Password must be at least 8 characters long.',
      })
      .min(8, { message: 'Password must be at least 8 characters long.' }),
    passwordConfirm: z.string({
      required_error: 'passwordConfirm is required',
    }),
  })
  .superRefine(({ password }, checkPassComplexity) => {
    if (!isStrongPassword(password))
      checkPassComplexity.addIssue({
        code: 'custom',
        path: ['password'],
        message: 'password does not meet complexity requirements',
      });
  })
  .refine(({ password, passwordConfirm }) => password === passwordConfirm, {
    message: "Passwords don't match",
    path: ['passwordConfirm'],
  });

type RegisterSchemaType = z.infer<typeof registerSchema>;

export default function SignUp() {
  const navigate = useNavigate();
  const { setSnackbarState, setLoading, loading } = useStore();
  const {
    handleSubmit,
    control,
    formState: { errors },
  } = useForm<RegisterSchemaType>({
    resolver: zodResolver(registerSchema),
    mode: 'all',
    reValidateMode: 'onChange',
  });

  const onSubmit = async ({
    email,
    name,
    password,
    passwordConfirm,
  }: RegisterSchemaType) => {
    try {
      const req = {
        name,
        email,
        password,
        passwordConfirm,
      };
      setLoading(true);
      await signUp(req);
      navigate('/');
    } catch (err) {
      console.log(err);
      if (err instanceof AxiosError) {
        const message = err.response?.data.message;
        setSnackbarState({
          message: message ?? err.message,
          severity: 'error',
        });
        return;
      }
      setSnackbarState({ message: 'Sign up failed', severity: 'error' });
    } finally {
      setLoading(false);
    }
  };

  return (
    <AuthLayout title="Sign up">
      <form
        style={{ width: '100%', marginTop: 16 }}
        onSubmit={handleSubmit(onSubmit)}
      >
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <Controller
              control={control}
              name="name"
              render={({ field }) => (
                <TextField
                  autoComplete="name"
                  inputRef={field.ref}
                  {...field}
                  variant="outlined"
                  fullWidth
                  label="Full Name"
                  autoFocus
                  error={!!errors.name?.message}
                  helperText={errors.name?.message}
                />
              )}
            />
          </Grid>
          <Grid item xs={12}>
            <Controller
              control={control}
              name="email"
              render={({ field }) => (
                <TextField
                  inputRef={field.ref}
                  {...field}
                  variant="outlined"
                  fullWidth
                  label="Email Address"
                  autoComplete="email"
                  error={!!errors.email?.message}
                  helperText={errors.email?.message}
                />
              )}
            />
          </Grid>
          <Grid item xs={12}>
            <Controller
              control={control}
              name="password"
              render={({ field }) => (
                <TextField
                  inputRef={field.ref}
                  {...field}
                  variant="outlined"
                  fullWidth
                  label="Password"
                  type="password"
                  error={!!errors.password?.message}
                  helperText={errors.password?.message}
                />
              )}
            />
          </Grid>
          <Grid item xs={12}>
            <Controller
              control={control}
              name="passwordConfirm"
              render={({ field }) => (
                <TextField
                  inputRef={field.ref}
                  {...field}
                  variant="outlined"
                  fullWidth
                  label="PasswordConfirm"
                  type="password"
                  error={!!errors.passwordConfirm?.message}
                  helperText={errors.passwordConfirm?.message}
                />
              )}
            />
          </Grid>
        </Grid>
        <Button
          type="submit"
          fullWidth
          variant="contained"
          color="primary"
          sx={{ marginTop: 2 }}
          disabled={loading}
        >
          Sign Up
        </Button>
        <Grid container sx={{ marginTop: 1 }}>
          <Grid item>
            <Link to="/login">
              <Typography variant="body2">
                Already have an account? Sign in
              </Typography>
            </Link>
          </Grid>
        </Grid>
      </form>
    </AuthLayout>
  );
}
