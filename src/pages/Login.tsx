import { useState } from 'react';
import { useNavigate, Link } from 'react-router-dom';
import { Grid, Typography, TextField, Button } from '@mui/material';
import { AxiosError } from 'axios';
import validator from 'validator';
import useStore from '../store';
import { login } from '../services/apiService';
import AuthLayout from '../layouts/AuthLayout';

export default function LoginPage() {
  const navigate = useNavigate();
  const setSnackbarState = useStore((state) => state.setSnackbarState);
  const setLoading = useStore((state) => state.setLoading);
  const loading = useStore((state) => state.loading);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  //Validation states
  const [emailValid, setEmailValid] = useState(true);

  const handleEmailChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setEmail(e.target.value);
    if (!emailValid && validator.isEmail(e.target.value)) {
      setEmailValid(true);
    }
  };

  const handlePasswordChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setPassword(e.target.value);
  };

  const handleSubmit = async (e: React.MouseEvent<HTMLButtonElement>) => {
    e.preventDefault();
    try {
      setLoading(true);
      await login(email, password);
      navigate('/');
    } catch (err) {
      if (err instanceof AxiosError) {
        const message = err.response?.data.message;
        setSnackbarState({
          message: message ?? err.message,
          severity: 'error',
        });
        return;
      }
      setSnackbarState({ message: 'Sign in failed', severity: 'error' });
    } finally {
      setLoading(false);
    }
  };

  const handleEmailBlur = () => {
    if (!validator.isEmail(email)) {
      setEmailValid(false);
    }
  };

  return (
    <AuthLayout title="Sign in">
      <form style={{ width: '100%', marginTop: 16 }} noValidate>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <TextField
              variant="outlined"
              required
              fullWidth
              id="email"
              label="Email Address"
              name="email"
              autoComplete="email"
              onChange={handleEmailChange}
              onBlur={handleEmailBlur}
              error={!emailValid}
              helperText={emailValid ? undefined : 'Invalid email'}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              variant="outlined"
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
              onChange={handlePasswordChange}
            />
          </Grid>
        </Grid>
        <Button
          type="submit"
          fullWidth
          variant="contained"
          color="primary"
          sx={{ marginTop: 2 }}
          onClick={handleSubmit}
          disabled={!email || !password || !emailValid || loading}
        >
          Sign In
        </Button>
        <Grid container sx={{ marginTop: 1 }}>
          <Grid item>
            <Link to="/signup">
              <Typography variant="body2">
                Don't have an account yet? Sign up
              </Typography>
            </Link>
          </Grid>
        </Grid>
      </form>
    </AuthLayout>
  );
}
