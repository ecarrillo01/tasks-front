import { Container } from '@mui/material';
import Tasks from '../components/tasks';
import AppBar from '../components/common/AppBar';

export default function Home() {
  return (
    <>
      <AppBar />
      <Container>
        <Tasks />
      </Container>
    </>
  );
}
