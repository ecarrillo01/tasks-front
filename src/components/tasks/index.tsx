/* eslint-disable @typescript-eslint/no-explicit-any */
import { Ref, useEffect, useRef } from 'react';
import { Box, Button, Grid } from '@mui/material';
import useTasksStore from '../../store';
import { Task, UpsertTaskData } from '../../services/apiService';
import * as apiService from '../../services/apiService';
import TaskCard from './TaskCard';
import TaskModal, { RefProps } from './TaskModal';
import { AxiosError } from 'axios';
import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';

export default function Tasks() {
  const modalRef: Ref<RefProps> | null = useRef(null);
  const tasks = useTasksStore((state) => state.tasks);
  const setTasks = useTasksStore((state) => state.setTasks);
  const setLoading = useTasksStore((state) => state.setLoading);
  const setSnackbarState = useTasksStore((state) => state.setSnackbarState);

  const onNewTask = () => modalRef.current?.createTask();
  const onEditTask = (task: Task) => modalRef.current?.editTask(task);

  const queryClient = useQueryClient();
  const tasksQuery = useQuery({
    queryKey: ['todos'],
    queryFn: apiService.getAllTasks,
  });

  const createTaskMutation = useMutation({
    mutationFn: apiService.createTask,
    onMutate: async (newTodo) => {
      // Cancel any outgoing refetches
      // (so they don't overwrite our optimistic update)
      await queryClient.cancelQueries({ queryKey: ['todos'] });

      // Snapshot the previous value
      const previousTodos = queryClient.getQueryData(['todos']);

      // Optimistically update to the new value
      queryClient.setQueryData(['todos'], (old: any) => [
        ...(old as any),
        { id: 'pending', ...newTodo },
      ]);

      // Return a context object with the snapshotted value
      return { previousTodos };
    },
    onSettled: async () =>
      await queryClient.invalidateQueries({ queryKey: ['todos'] }),
    onError(e, _, context) {
      queryClient.setQueryData(
        ['todos'],
        (context?.previousTodos as any) ?? []
      );
      const msg =
        e instanceof AxiosError
          ? e.response?.data.message ?? e.message
          : 'Error while creating task';
      setSnackbarState({
        message: msg,
        severity: 'error',
      });
    },
  });

  const updateTaskMutation = useMutation({
    mutationFn: ({ id, ...rest }: Task) => apiService.updateTask(id, rest),
    onSettled: () => queryClient.invalidateQueries({ queryKey: ['todos'] }),
    onMutate: async (newTodo) => {
      await queryClient.cancelQueries({ queryKey: ['todos'] });
      const previousTodos = queryClient.getQueryData(['todos']);
      queryClient.setQueryData(['todos'], (old: Task[]) =>
        old.map((item) => (item.id === newTodo.id ? newTodo : item))
      );
      return { previousTodos };
    },
    onError(e, _, context) {
      queryClient.setQueryData(
        ['todos'],
        (context?.previousTodos as any) ?? []
      );
      const msg =
        e instanceof AxiosError
          ? e.response?.data.message ?? e.message
          : 'Error while updating task';
      setSnackbarState({
        message: msg,
        severity: 'error',
      });
    },
  });

  const deleteTaskMutation = useMutation({
    mutationFn: apiService.deleteTask,
    onSettled: () => queryClient.invalidateQueries({ queryKey: ['todos'] }),
    onMutate: async (id) => {
      await queryClient.cancelQueries({ queryKey: ['todos'] });
      const previousTodos = queryClient.getQueryData(['todos']);
      queryClient.setQueryData(['todos'], (old: Task[]) =>
        old.filter((item) => item.id !== id)
      );
      return { previousTodos };
    },
    onError(e, _, context) {
      queryClient.setQueryData(
        ['todos'],
        (context?.previousTodos as any) ?? []
      );
      const msg =
        e instanceof AxiosError
          ? e.response?.data.message ?? e.message
          : 'Error while deleting task';
      setSnackbarState({
        message: msg,
        severity: 'error',
      });
    },
  });

  const onSaveEdited = async (taskId: string, data: UpsertTaskData) =>
    updateTaskMutation.mutate({ ...data, id: taskId });

  useEffect(() => {
    return () => {
      queryClient.removeQueries({ queryKey: ['todos'] });
      setTasks([]);
    };
  }, [queryClient, setTasks]);

  // Update task mutations
  useEffect(() => {
    setLoading(updateTaskMutation.isPending);
  }, [updateTaskMutation.isPending, setLoading]);

  // Delete task mutations
  useEffect(() => {
    setLoading(deleteTaskMutation.isPending);
  }, [deleteTaskMutation.isPending, setLoading]);

  useEffect(() => {
    setLoading(tasksQuery.isLoading);
    if (tasksQuery.isSuccess) {
      return setTasks(tasksQuery.data);
    }
  }, [
    setLoading,
    setTasks,
    tasksQuery.data,
    tasksQuery.isLoading,
    tasksQuery.isSuccess,
  ]);

  return (
    <Box sx={{ marginBottom: 4 }}>
      <Button variant="outlined" sx={{ marginBottom: 1.5 }} onClick={onNewTask}>
        New Task
      </Button>
      <Grid container rowSpacing={2.5} columnSpacing={1}>
        {tasks.map((task) => {
          return (
            <Grid item lg={4} md={4} xs={12} sm={12} key={task.id}>
              <TaskCard
                task={task}
                onEdit={onEditTask}
                onDelete={deleteTaskMutation.mutate}
              />
            </Grid>
          );
        })}
      </Grid>
      <TaskModal
        ref={modalRef}
        onSaveCreated={createTaskMutation.mutate}
        onSaveEdited={onSaveEdited}
      />
    </Box>
  );
}
