import { createRef, forwardRef, useImperativeHandle, useState } from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import { Task, UpsertTaskData } from '../../services/apiService';
import { TextField } from '@mui/material';
import { DatePicker } from '@mui/x-date-pickers';

export interface RefProps {
  createTask(): void;
  editTask(task: Task): void;
}

interface Props {
  onClose?: (task: Task) => void;
  onSaveCreated?: (task: UpsertTaskData) => void;
  onSaveEdited?: (taskId: string, task: UpsertTaskData) => void;
}

const TaskModal = forwardRef<RefProps, Props>(
  ({ onSaveCreated, onSaveEdited }, ref) => {
    const formRef = createRef<HTMLFormElement>();
    const [id, setId] = useState<string | null>();
    const [title, setTitle] = useState<string>('');
    const [dueDate, setDueDate] = useState<Date>(new Date());
    const [description, setDescription] = useState<string>('');
    const [open, setOpen] = useState(false);

    const handleDueDateChange = (newValue: Date | null) => {
      if (newValue) {
        setDueDate(newValue);
      }
    };

    const clear = () => {
      setId(null);
      setTitle('');
      setDescription('');
      setDueDate(new Date());
    };

    const handleClose = () => {
      setOpen(false);
      clear();
    };

    useImperativeHandle(
      ref,
      () => {
        return {
          createTask: () => {
            setId(null);
            setOpen(true);
          },
          editTask: (task) => {
            setId(task.id);
            setTitle(task.title);
            setDescription(task.description);
            setDueDate(task.dueDate);
            setOpen(true);
          },
        };
      },
      []
    );

    return (
      <>
        <Dialog
          open={open}
          onClose={handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <form
            ref={formRef}
            onSubmit={(e) => {
              e.preventDefault();
            }}
          >
            <DialogTitle id="alert-dialog-title">
              {id ? 'Edit task' : 'Create task'}
            </DialogTitle>
            <DialogContent>
              <DialogContentText id="alert-dialog-description">
                <TextField
                  value={title}
                  onChange={(e) => {
                    setTitle(e.target.value);
                  }}
                  autoFocus
                  margin="dense"
                  label="Title"
                  type="text"
                  fullWidth
                  variant="outlined"
                />
                <TextField
                  value={description}
                  onChange={(e) => {
                    setDescription(e.target.value);
                  }}
                  margin="dense"
                  label="Description"
                  type="text"
                  fullWidth
                  variant="outlined"
                />
                <DatePicker
                  label="Due date"
                  sx={{ width: '100%', mt: 2 }}
                  value={dueDate}
                  onChange={handleDueDateChange}
                />
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={handleClose}>Cancel</Button>
              <Button
                onClick={() => {
                  handleClose();
                  if (id) {
                    onSaveEdited?.(id, {
                      title,
                      description,
                      dueDate,
                    });
                  } else {
                    onSaveCreated?.({
                      title,
                      description,
                      dueDate,
                    });
                  }
                }}
                autoFocus
                disabled={!title || !description}
              >
                Save
              </Button>
            </DialogActions>
          </form>
        </Dialog>
      </>
    );
  }
);

export default TaskModal;
