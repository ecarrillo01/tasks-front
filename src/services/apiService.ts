import axios, { AxiosError } from 'axios';

const axiosInstance = axios.create({
  baseURL: `${import.meta.env.VITE_API_URL}`,
  timeout: 10000,
});

interface SignUpReq {
  name: string;
  email: string;
  password: string;
  passwordConfirm: string;
}

export interface Task {
  id: string;
  title: string;
  description: string;
  dueDate: Date;
}

export type UpsertTaskData = Pick<Task, 'title' | 'description' | 'dueDate'>;

const localToken = (() => {
  const key = 'access_token';
  return {
    create(token: string) {
      return localStorage.setItem(key, token);
    },
    get() {
      return localStorage.getItem(key);
    },
    rm() {
      localStorage.removeItem(key);
    },
  };
})();

axiosInstance.interceptors.response.use(
  function (response) {
    return response;
  },
  function (error) {
    if (error instanceof AxiosError && error.response?.status === 401) {
      localToken.rm();
    }
    return Promise.reject(error);
  }
);

export const isAuthenticated = () => {
  const token = localToken.get();
  if (token) {
    axiosInstance.defaults.headers.common['Authorization'] = `Bearer ${token}`;
    return true;
  }
  return false;
};

export const signUp = async (body: SignUpReq) => {
  const res = await (
    await axiosInstance.post<Promise<{ token: string }>>('/auth/signup', body)
  ).data;
  localToken.create(res.token);
  axiosInstance.defaults.headers.common[
    'Authorization'
  ] = `Bearer ${res.token}`;
  return res.token;
};

export const login = async (email: string, password: string) => {
  const res = await (
    await axiosInstance.post<Promise<{ token: string }>>('/auth/login', {
      email,
      password: password,
    })
  ).data;
  localToken.create(res.token);
  axiosInstance.defaults.headers.common[
    'Authorization'
  ] = `Bearer ${res.token}`;
  return res.token;
};

export const logOut = async () => {
  const res = await await axiosInstance.delete<Promise<{ token: string }>>(
    '/auth/logout'
  );
  if (res.status === 200) {
    localToken.rm();
  }
  return true;
};

export const getAllTasks = async () => {
  const res = (await axiosInstance.get<Promise<Task[]>>('/tasks')).data;
  const formattedRes = (await res).map((task) => {
    return {
      ...task,
      dueDate: new Date(task.dueDate),
    };
  });
  return formattedRes;
};

export const createTask = async (data: UpsertTaskData) => {
  const res = (await axiosInstance.post<Promise<Task>>(`/tasks`, data)).data;
  return res;
};

export const updateTask = async (id: string, data: UpsertTaskData) => {
  const res = (await axiosInstance.patch<Promise<Task>>(`/tasks/${id}`, data))
    .data;
  return res;
};

export const deleteTask = async (id: string) => {
  return (await axiosInstance.delete(`/tasks/${id}`)).data;
};
