import { Task, getAllTasks } from './services/apiService';
import { AlertColor } from '@mui/material';
import { AxiosError } from 'axios';
import { create } from 'zustand';

type SnackbarState = {
  message: string;
  severity: AlertColor;
} | null;

interface Store {
  loading: boolean;
  setLoading(value: boolean): void;
  tasks: Task[];
  setTasks(newTasks: Task[]): void;
  addTask(task: Task): void;
  snackbarState: SnackbarState;
  setSnackbarState: (newSnackbarState: SnackbarState) => void;
  getAllTasksDeprecated: () => void;
}

const useTasksStore = create<Store>((set) => ({
  loading: false,
  setLoading: (value) => set(() => ({ loading: value })),
  tasks: [],
  setTasks: (newTasks) => set(() => ({ tasks: newTasks })),
  addTask: (newTask) => set((state) => ({ tasks: [...state.tasks, newTask] })),
  snackbarState: null,
  setSnackbarState: (newSnackbarState) =>
    set(() => ({ snackbarState: newSnackbarState })),
  getAllTasksDeprecated: async () => {
    try {
      set(() => ({ loading: true }));
      const res = await getAllTasks();
      set(() => ({ tasks: res, loading: false }));
    } catch (e) {
      const msg =
        e instanceof AxiosError
          ? e.response?.data.message ?? e.message
          : 'Error getting tasks';
      set(() => ({
        loading: false,
        snackbarState: { message: msg, severity: 'error' },
      }));
      console.log(e);
    }
  },
}));

export default useTasksStore;
